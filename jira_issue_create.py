"""

"""
import re
import sys
import json

cmd_args = sys.argv

jsonStr = cmd_args[1]
jsonStr = jsonStr.replace("https:","")
jsonStr = re.sub("((?=\D)\w+):", r'"\1":',  jsonStr)
jsonStr = re.sub(": ((?=\D)\w+)", r':"\1"',  jsonStr)

#input_testplan_json_object = json.loads(jsonStr)

print(f"jsonStr:{jsonStr}")

baseURL = "https://prudhviakella.atlassian.net"
xray_cloud_base_url = "https://xray.cloud.getxray.app/api/v1"
xray_cloud_base_graphql_url = "https://xray.cloud.getxray.app/api/v2/graphql"
client_id = "DCA365558DFE4948937A794DB8FDB9D0"
client_secret = "08bea73353e55a74fa3d71ded3c870b926f4f7cde438e6e4bb9a23a72d3345f7"

import json
import requests
import traceback

auth_user = "prudhvi.prudhvi.akella@gmail.com"
auth_pass = "w1MPW3M9jKW0HDJ4aLjcAB06"


def get_headers():
    return {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }


def get_xray_project_name():
    return "Xray Testcases"


def get_credentials():
    return auth_user, auth_pass


def get_dashboards():
    url = baseURL + '/rest/agile/1.0/board'
    response = serve_get_request(url)
    return response


def get_xray_dashboard():
    global dashboard
    xray_test_cases_board_id = 0
    dashboard_response = get_dashboards()
    if dashboard_response['status_code'] == 200:
        xray_dashboard_found = False
        dashboards = dashboard_response['values']
        if len(dashboards) > 0:
            for dashboard in dashboards:
                project_name = (dashboard['location'])['projectName']
                if project_name == get_xray_project_name():
                    xray_dashboard_found = True
                    xray_test_cases_board_id = dashboard['id']
                    break
        if xray_dashboard_found:
            return dashboard
        else:
            unable_to_process_request({'message': 'unable to find {} dashboard'.format(get_xray_project_name())})
    else:
        unable_to_process_request(dashboard_response)


def unable_to_process_request(message):
    print("Unable to process the request due to:")
    print(message)
    exit(1)


def get_sprints(dashboard_id):
    url = baseURL + f'/rest/agile/1.0/board/{dashboard_id}/sprint'
    response = serve_get_request(url)
    return response


def get_active_sprint():
    dashboard = get_xray_dashboard()
    dashboard_id = dashboard['id']
    sprints_response = get_sprints(dashboard_id)
    print(sprints_response)


def create_issue(payload):
    request_url = baseURL + "/rest/api/2/issue"
    response = server_post_request(request_url, payload)
    return response


def serve_get_request(url):
    final_json = {}
    try:
        response = requests.get(url=url, headers=get_headers(), auth=get_credentials())
        final_json = json.loads(response.text)
        final_json['status_code'] = response.status_code
        final_json['reason'] = response.reason
        final_json['error'] = False
    except Exception as ex:
        final_json['error'] = True
        final_json['exp_msg']: traceback.print_exc()
    return final_json


def server_post_request(request_url, data):
    final_json = {}
    try:
        response = requests.post(
            request_url,
            auth=get_credentials(),
            headers=get_headers(),
            data=json.dumps(data))
        final_json = json.loads(response.text)
        final_json['status_code'] = response.status_code
        final_json['reason'] = response.reason
        final_json['error'] = False
    except Exception as ex:
        final_json['error'] = True
        final_json['exp_msg']: traceback.print_exc()
    return final_json


def server_put_request(request_url, data):
    final_json = {}
    try:
        response = requests.post(
            request_url,
            auth=get_credentials(),
            headers=get_headers(),
            data=json.dumps(data))
        final_json = json.loads(response.text)
        final_json['status_code'] = response.status_code
        final_json['reason'] = response.reason
        final_json['error'] = False
    except Exception as ex:
        final_json['error'] = True
        final_json['exp_msg']: traceback.print_exc()
    return final_json


def get_xray_cloud_headers(auth_token):
    headers = {'Authorization': 'Bearer ' + auth_token, 'Content-type': 'application/json'}
    return headers


def server_xray_graphql_post_request(request_url, query, auth_token):
    final_json = {}
    try:
        response = requests.post(
            request_url,
            headers=get_xray_cloud_headers(auth_token),
            json={'query': query}
        )
        final_json = json.loads(response.text)
        final_json['status_code'] = response.status_code
        final_json['reason'] = response.reason
        final_json['error'] = False
    except Exception as ex:
        final_json['error'] = True
        final_json['exp_msg'] = traceback.print_exc()
    return final_json


def server_xray_post_request(request_url, data, auth_token):
    final_json = {}
    print(request_url)
    print(data)
    print(get_xray_cloud_headers(auth_token))
    try:
        response = requests.post(
            request_url,
            headers=get_xray_cloud_headers(auth_token),
            data=json.dumps(data)
        )
        print(response.text)
        final_json = json.loads(response.text)
        final_json['status_code'] = response.status_code
        final_json['reason'] = response.reason
        final_json['error'] = False
    except Exception as ex:
        final_json['error'] = True
        final_json['exp_msg'] = traceback.print_exc()
    return final_json


def create_test_plan(project_key):
    payload = {
        "fields": {
            "project":
                {
                    "key": project_key
                },
            "summary": "Test Plan",
            "description": "example of cucumber automated test - Scenario",
            "issuetype": {
                "name": "Test Plan"
            },

        }
    }
    response = create_issue(payload)
    print(response)
    return response


def create_xray_test(project_key, xray_auth_token):
    try:
        query = """
        
                mutation {
            createTest(
                testType: { name: "Generic" },
                unstructured: "Perform exploratory tests on calculator.",
                jira: {
                    fields: { summary:"Exploratory Test", project: {key: "%s"} }
                }
            ) {
                test {
                    issueId
                    testType {
                        name
                    }
                    unstructured
                    jira(fields: ["key"])
                }
                warnings
            }
        }
      """ % (project_key)
        response = server_xray_graphql_post_request(xray_cloud_base_graphql_url, query, xray_auth_token)
        print(response)
        return response
    except Exception as ex:
        print(traceback.print_exc())


def create_xray_testplan(project_key, testids, xray_auth_token):
    try:
        tmp_str = ""
        len_items = len(testids) - 1
        item_itr = 0
        for testkey, testid in testids:
            if item_itr != len_items:
                tmp_str = tmp_str + f'"{testid}",'
            else:
                tmp_str = tmp_str + f'"{testid}"'
            item_itr = item_itr + 1
        query = """
            mutation {
                createTestPlan(
                    testIssueIds: [%s]
                    jira: {
                        fields: { summary: "Test Plan for v1.0", project: {key: "%s"} }
                    }
                ) {
                    testPlan {
                        issueId
                        jira(fields: ["key"])
                    }
                    warnings
                }
            }
        """ % (tmp_str, project_key)
        response = server_xray_graphql_post_request(xray_cloud_base_graphql_url, query, xray_auth_token)
        print(response)
        return response
    except Exception as ex:
        print(traceback.print_exc())


def update_test_plan(testPlanKey, testkeys, xray_auth_token):
    tmp_str = ""
    print("Inside update_test_plan")
    print(testPlanKey)
    print(testkeys)
    len_items = len(testkeys) - 1
    item_itr = 0
    for testkey, testid in testkeys:
        if item_itr != len_items:
            tmp_str = tmp_str + f'"{testid}",'
        else:
            tmp_str = tmp_str + f'"{testid}"'
        item_itr = item_itr + 1
    url = xray_cloud_base_graphql_url
    query = """
    
        mutation {
            addTestsToTestPlan(
                issueId: "%s",
                testIssueIds: [%s]
            ) {
                addedTests
                warning
            }
        }""" % (testPlanKey, tmp_str)
    print(query)
    print(url)
    response = server_xray_graphql_post_request(url, query, xray_auth_token)
    print(response)


def import_xray_test_results(testplankey, testids, auth_token):
    try:
        tests = []
        for testkey, testid in testids:
            tests.append({
                "testKey": testkey,
                "status": "PASSED"
            })
        url = xray_cloud_base_url + '/import/execution'
        print(url)
        payload = {
            "info": {
                "summary": "Execution of automated tests for release v1.3",
                "testPlanKey": testplankey
            },
            "tests":tests
        }
        response = server_xray_post_request(url, payload, auth_token)
        print(response)
    except Exception as ex:
        pass


def get_xray_auth_token():
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    auth_data = {"client_id": client_id, "client_secret": client_secret}
    response = requests.post(f'{xray_cloud_base_url}/authenticate', data=json.dumps(auth_data), headers=headers)
    auth_token = response.json()
    return auth_token


test_list = []

xray_auth_token = get_xray_auth_token()
xray_dashboard = get_xray_dashboard()

if xray_dashboard['type'] == 'kanban':
    project_location = xray_dashboard['location']
    project_name = project_location['projectName']
    project_key = project_location['projectKey']
    for i in range(0, 2):
        xray_create_test_response = create_xray_test(project_key, xray_auth_token)
        print(xray_create_test_response)
        if xray_create_test_response['status_code'] == 200 or xray_create_test_response['status_code'] == 201:
            test_info = ((xray_create_test_response['data'])['createTest'])['test']
            test_key = (test_info['jira'])
            print(test_key)
            test_list.append((test_key['key'], test_info['issueId']))
    if len(test_list) > 0:
        create_xray_testplan_response = create_xray_testplan(project_key, test_list, xray_auth_token)
        if create_xray_testplan_response['status_code'] == 200 or create_xray_testplan_response['status_code'] == 201:
            test_plan_info = ((create_xray_testplan_response['data'])['createTestPlan'])['testPlan']
            test_plan_key = (test_plan_info['jira'])['key']
            import_xray_test_results(test_plan_key, test_list, xray_auth_token)